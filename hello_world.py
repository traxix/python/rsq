from traxix.rsq import RSQ, Const

# Init
rsq = RSQ(mongo_url="localhost")

# Push work
rsq.push(foo="bar")

# Get work
work = rsq.pull()
# do work
print(work[Const.DATA])

# Send result
rsq.done(_id=work[Const.ID], result=42)

# Cleanup
rsq.remove()
