import math
from traxix.rsq import RSQ, Const

from fire import Fire


class Prime:
    def __init__(self, mongo_url: str = "localhost:27017"):
        self.rsq = RSQ(mongo_url=mongo_url)

        # cleanup
        if self.rsq.count() != 0:
            print("base is dirty, cleaning up")
            self.rsq.remove()

        print(self.rsq.count())
        if self.rsq.count() != 0:
            raise RuntimeError(
                f"Task present after cleanup, we are not alone? {self.rsq.count()}"
            )

    def example(self):

        # push tasks
        self.rsq.push(random_key="random_value")

        print("tasks> todo>       ", self.rsq.list_task(state=Const.State.TODO))
        print("tasks> inprogress> ", self.rsq.list_task(state=Const.State.INPROGRESS))
        print("tasks> done>       ", self.rsq.list_task(state=Const.State.DONE))
        print("tasks> failed>     ", self.rsq.list_task(state=Const.State.FAIL))
        print("tasks> all>        ", self.rsq.list_task())

        # Consume task

        tasks = self.push_work()
        self.do_work()
        self.check(tasks)

    def check(self, tasks):
        print(tasks)
        assert self.rsq.count(state=Const.State.DONE) == len(tasks)

        results_0 = self.rsq.results(_id=tasks[0])[0]
        print(results_0)
        print(results_0[Const.RESULT])

        assert results_0[Const.RESULT] == [3, 5, 7, 11, 13, 17]

    def push_work(self):
        step = 17
        tasks = []
        for start in range(1, 100, step):
            task_id = self.rsq.push(start=start, stop=start + step)
            print(f"Pushing task> {start} - {start + step} - {task_id}")
            tasks.append(task_id)

        return tasks

    def do_work(self):

        query = {"data.start": {"$exists": True}, "data.stop": {"$exists": True}}
        work = self.rsq.pull(query)
        while work is not None:
            result = self._are_prime(
                start=work[Const.DATA]["start"], stop=work[Const.DATA]["stop"]
            )
            self.rsq.done(_id=work[Const.ID], result=result)
            print("Done ", work[Const.ID], result)
            work = self.rsq.pull(query)

    @staticmethod
    def _is_prime(n):
        if (n % 2) == 0 or n < 2:
            return False
        root = int(math.sqrt(n) + 1)
        for i in range(3, root, 2):
            if (n % i) == 0:
                return False
        return True

    @staticmethod
    def _are_prime(start, stop):
        primes = []
        if (start % 2) == 0:
            start += 1

        for n in range(start, stop, 2):
            if Prime._is_prime(n):
                primes.append(n)

        return primes


if __name__ == "__main__":
    Fire(Prime)
